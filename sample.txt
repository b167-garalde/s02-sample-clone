Hazel Garalde, Batch 167

I have always been passionate about coding. The intricate logic behind it has always mesmerized me and at the same time encouraged me to learn more. Aside from coding, I also love to learn the Japanese Language.